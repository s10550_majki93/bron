package database.builder;

import java.sql.ResultSet;

import model.EntityBase;

public interface IEntityBuilder<E extends EntityBase> {

	public E build(ResultSet rs);
	
}