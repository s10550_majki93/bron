package database;

import java.util.HashMap;
import java.util.Map;

import UOW.IUnitOfWork;
import UOW.IUnitOfWorkRepo;
import model.EntityBase;

public class UnitOfWork implements IUnitOfWork{
	
	private Map<EntityBase, IUnitOfWorkRepo> added;
	private Map<EntityBase, IUnitOfWorkRepo> deleted;
	private Map<EntityBase, IUnitOfWorkRepo> updated;
	
	public UnitOfWork(){
		added = new HashMap<EntityBase, IUnitOfWorkRepo>();
		deleted = new HashMap<EntityBase, IUnitOfWorkRepo>();
		updated = new HashMap<EntityBase, IUnitOfWorkRepo>();
		
	}
	
	public void RegisterAdd(EntityBase obj, IUnitOfWorkRepo repo) {
		added.put(obj, repo);
	
	}

	public void RegisterDelete(EntityBase obj, IUnitOfWorkRepo repo) {
		deleted.put(obj,  repo);
		
	}

	public void RegisterUpdate(EntityBase obj, IUnitOfWorkRepo repo) {
		updated.put(obj,  repo);
		
	}
	
	public void commit() {
		for(EntityBase ent:added.keySet())
		{
			added.get(ent).persistAdd(ent);
		}
		for(EntityBase ent:updated.keySet())
		{
			updated.get(ent).persistUpdate(ent);
		}
		for(EntityBase ent:deleted.keySet())
		{
			deleted.get(ent).persistDelete(ent);
		}
		added.clear();
		updated.clear();
		deleted.clear();
	}

}