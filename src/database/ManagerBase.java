package database;

import UOW.IUnitOfWork;
import UOW.IUnitOfWorkRepo;
import model.EntityBase;

public abstract class ManagerBase<T extends EntityBase> implements IManager<T>, IUnitOfWorkRepo{
	
	protected IUnitOfWork uow;
	
	public ManagerBase(IUnitOfWork uow){
		super();
		this.uow = uow;
	}
	
	public void add(T obj){
		uow.RegisterAdd(obj, this);
	
	}
	
	public void delete(T obj) {
		uow.RegisterDelete(obj, this);
		
	}
	
	public void update(T obj){
		uow.RegisterUpdate(obj, this);
	}
	
	public final void saveChanges()
	{
		uow.commit();
	}
		
	
	public abstract void persistAdd(EntityBase obj);
	public abstract void persistDelete(EntityBase obj);
	public abstract void persistUpdate(EntityBase obj);
}