package database;

import java.util.List;

import model.EntityBase;


public interface IManager<T extends EntityBase> {
	public T get(int id);
	public List<T> getAll(PageInfo request);
	public void add(T obj);
	public void delete(T obj);
	public void update(T obj);
	
}