package model;

public class Bron {

	int id;
	private String nazwa;
	private String nr_seryjny;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public String getNr_seryjny() {
		return nr_seryjny;
	}
	public void setNr_seryjny(String nr_seryjny) {
		this.nr_seryjny = nr_seryjny;
	}
	
	
}
