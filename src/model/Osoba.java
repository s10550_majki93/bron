package model;
import java.util.Date;

public class Osoba {
	
	private String imie;
	private String nazwisko;
	private int telefon;
	private Date du = new Date();
	private Date dd = new Date();
	private int pensja;
	private int pesel;
	
	
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public int getTelefon() {
		return telefon;
	}
	public void setTelefon(int telefon) {
		this.telefon = telefon;
	}
	public Date getDu() {
		return du;
	}
	public void setDu(Date du) {
		this.du = du;
	}
	public Date getDd() {
		return dd;
	}
	public void setDd(Date dd) {
		this.dd = dd;
	}
	public int getPensja() {
		return pensja;
	}
	public void setPensja(int pensja) {
		this.pensja = pensja;
	}
	public int getPesel() {
		return pesel;
	}
	public void setPesel(int pesel) {
		this.pesel = pesel;
	}




}

