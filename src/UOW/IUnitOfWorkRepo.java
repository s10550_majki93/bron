package UOW;

import model.EntityBase;

public interface IUnitOfWorkRepo {
	public void persistAdd(EntityBase obj);
	public void persistDelete(EntityBase obj);
	public void persistUpdate(EntityBase obj);
	
}