package UOW;

import model.EntityBase;

public interface IUnitOfWork {
	public void RegisterAdd(EntityBase obj, IUnitOfWorkRepo repo);
	public void RegisterDelete(EntityBase obj, IUnitOfWorkRepo repo);
	public void RegisterUpdate(EntityBase obj, IUnitOfWorkRepo repo);
	public void commit();
}